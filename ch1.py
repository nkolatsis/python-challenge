__author__ = 'Nicholas Kolatsis'
import string

class Main(object):

    text = '''g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj.'''

    ans = ''
    def solution_one(self):
        # Original solution
        text = list(self.text)

        ans = ''
        exclude = ['.',' ','(',')',"'",'y','z']
        for c in text:
            flag = False
            for e in exclude:
                if c is 'y':
                    ans = ans + 'a'
                    flag=True
                    break
                if c is 'z':
                    ans = ans + 'b'
                    flag = True
                    break
                if c is e:
                    ans = ans + c
                    flag = True
                    break
            if flag:
                continue
            ans = ans+(chr(ord(c)+2))

        print ans

    def solution_two(self):
        # Second solution containing string.maketrans() as recommended by solved text
        intab = '''abcdefghijklmnopqrstuvwxyz'''
        outtab = '''cdefghijklmnopqrstuvwxyzab'''
        translation_table = string.maketrans(intab,outtab)

        print self.text.translate(translation_table)

Main().solution_two()

