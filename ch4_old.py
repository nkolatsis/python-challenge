__author__ = 'Nicholas Kolatsis'
__comment__ = '''
http://www.pythonchallenge.com/pc/def/linkedlist.php
'''
import urllib2
import re

class Challenge4(object):

    def go(self):
        url = 'http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=25357'
        regex = re.compile('[\d]+')
        for i in range(1,450):
            response = urllib2.urlopen(url)
            html =  response.read()
            s = regex.search(html)
            if s is not None:
                number = html[s.start():s.end()]
                url = 'http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing='+number
                print ''.join([str(i),': ', str(number)])

Challenge4().go()
