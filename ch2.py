__author__ = 'Nicholas Kolatsis'
__comment__ = '''

'''
import challenge_resources

class Solution_A(object):
    text = ''
    strip_these = ['#','$','[',']','(',')','{','}','%','@','_','^','&','!','+','*','\n']
    def __init__(self):
        self.text = challenge_resources.Challenge3().get_text()

    def strip_down(self):
        build_me = []
        for c in list(self.text):
            strip_char = False
            for d in self.strip_these:
                if c == d:
                    strip_char = True
                    break
            if not strip_char:
                build_me.append(c)

        return str(build_me)

print Solution_A().strip_down()